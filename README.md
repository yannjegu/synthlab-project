# Synthlab Project

# The project
The software can synthesize audio streams using a set of audio processing modules. The user can connect and adjust the various modules.

The software to perform a null simulation of analog hardware architecture known as synthesizer analog to subtractive synthesis.

# Team: Les Saints Tétiseurs
- Yann Jegu
- Ronan Cadoret
- Quentin Guillou
- Annaig Butterworth
- Jimmy Brulez